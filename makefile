all:
	##logicStatements
	javac pooabc/logicManager/Bean.java
	javac pooabc/logicManager/Atomizer.java
	javac pooabc/logicManager/SuperBuffer.java
	##fileStatements
	javac pooabc/fileManager/Reader.java
	javac pooabc/fileManager/Writer.java
	##ViewStatements
	#mainStatement
	javac pooabc/Main.java
# the execute statements
	java pooabc.Main
