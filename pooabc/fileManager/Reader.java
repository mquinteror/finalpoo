package pooabc.fileManager;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class Reader{

    File imputFile = null;
    FileReader readFile = null;
    BufferedReader bufferedFile = null;

    // constructor
    public Reader () {

	try {
	    imputFile = new File("./pooabc/data/database.txt");
	    readFile = new FileReader ( imputFile );
	    bufferedFile = new BufferedReader ( readFile );
	} catch ( Exception e ){
	    System.out.println("[X] - No se ha podido preparar para la lectura");
	}
    }
    
    public String getLine(){
	String line = null;
	try {
	    line = bufferedFile.readLine();
	}
	catch ( Exception e ){
	    System.out.println("[X] - No se ha podido leer el archivo");
	    line = null;
	}
	return line;
    }
}
