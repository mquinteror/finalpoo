package pooabc.fileManager;

import java.io.FileWriter;
import java.io.PrintWriter;


public class Writer{
    // this class writer on the text file;

    FileWriter file = null;
    PrintWriter writer = null;
    String line = null;
    
    public Writer (){
	try{
	this.file = new FileWriter("./pooabc/data/database1.txt");
        this.writer = new PrintWriter ( file );
	}
	catch ( Exception e ){
	    System.out.println("[X] - No se pudo preparar para leer");
	}
      
    }

    public void printBean ( pooabc.logicManager.Bean bean ){
	pooabc.logicManager.Atomizer atom = new pooabc.logicManager.Atomizer(bean);
	this.line = atom.beantoLine();

	try{
	    this.writer.println(this.line);
	}
	catch (Exception e ) {
	    System.out.println("[X] - No se pudo escribir");
	}
	finally {
	    try {
		if ( file != null ){
		    file.close();
		}
	    }
	    catch (Exception e2){
		System.out.println("[X] - No se pudo cerrar");
	    }
	}
    }
    
}
