package pooabc.logicManager;

public class Bean {
    // this class contains structured informationn of atomic element
    // data
    String name = null;
    String firstName = null;
    String secondName = null;
    String login = null;
    String combination = null;
    String userType = null;

    public Bean (){
	// nothing to do
    }

    public Bean ( String name, String firstName, String secondName,
		  String login, String combination, String userType){
	this.name = name;
	this.firstName = firstName;
	this.secondName = secondName;
	this.login = login;
	this.combination = combination;
	this.userType = userType;
    }

    public void setName ( String name ){
	this.name = name;
    }
    public String getName (){
	return this.name;
    }


    public void setFirstName( String firstName ){
	this.firstName = firstName;
    }
    public String getFirstName(){
	return this.firstName;
    }

    public void setSecondName( String secondName ){
	this.secondName = secondName;
    }
    public String getSecondName(){
	return this.secondName;
    }

    public void setLogin ( String login ){
	this.login = login;
    }
    public String getLogin(){
	return this.login;
    }

    public void setCombination ( String combination ){
	this.combination = combination;
    }
    public String getCombination(){
	return this.combination;
    }

    public void setUserType ( String userType ){
	this.userType = userType;
    }
    public String getUserType(){
	return userType;
    }
}
